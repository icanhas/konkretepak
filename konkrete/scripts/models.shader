// this file contains shaders attached to models

//=====================================================================
// flags
//=====================================================================

models/flags/b_flag
{
	cull none
	{
		map models/flags/b_flag
		rgbgen lightingdiffuse
	}
}

models/flags/r_flag
{
	cull none
	{
		map models/flags/r_flag
		rgbgen lightingdiffuse
	}
}

//=====================================================================
// missiles
//=====================================================================

models/missiles/tracer
{
	cull none
	sort nearest
	{
		map models/missiles/tracer
		blendfunc add
	}
}

models/missiles/tracerradius
{
	cull none
	sort nearest
	{
		map models/missiles/tracerradius
		blendfunc add
	}
}

models/missiles/rocketflare
{
	cull none
	sort nearest
	{
		map models/missiles/rocketflare
		blendfunc alphaadd
		//blendfunc GL_SRC_ALPHA GL_ONE
		//alphafunc GT0
	}
}

models/missiles/grenade
{
	{
		map models/missiles/grenade
		rgbgen identity
	}
}

models/missiles/rocketnade
{
	{
		map models/missiles/rocketnade
		rgbgen lightingdiffuse
	}
	{
		stage specularmap
		map models/missiles/rocketnade_s
		specularreflectance 0.56275
	}
	{
		// glow
		map models/missiles/rocketnade_g
		blendfunc alphaadd
		rgbgen identity
	}
}

models/missiles/rockethoming
{
	{
		map $whiteimage
		rgbgen const ( .7 .15 .15 )
	}
	{
		map textures/fx/sabfx3.tga
		blendfunc add
		tcGen environment
	}
}

models/missiles/plasmaball
{
	cull none
	{
		clampmap models/missiles/plasmaball
		blendfunc alphaadd
	}
}

models/missiles/plasmatail
{
	cull none
	{
		clampmap models/missiles/plasmatail
		blendfunc alphaadd
	}
}

models/missiles/seekerflare
{
	cull none
	deformVertexes autoSprite
	{
		clampmap models/missiles/seekerflare_g
		blendfunc alphaadd
		rgbgen identity
	}
}

//=====================================================================
// players
//=====================================================================

models/players/thrust
{
	cull none
	{
		map models/players/thrust
		blendfunc add
		rgbgen identity
	}
}

models/players/griever/enginebright
{
	{
		map models/players/griever/engine
		rgbgen lightingdiffuse
	}
	{
		map models/players/griever/enginebright
		blendfunc blend
		//alphafunc ge128		// may need this
		rgbgen entity			// cg_enemycolor/cg_teamcolor
	}
}

models/players/griever/shipbodybright
{
	{
		map models/players/griever/shipbody
		rgbgen lightingdiffuse
	}
	{
		map models/players/griever/shipbodybright
		blendfunc blend
		//alphafunc ge128		// may need this
		rgbgen entity			// cg_enemycolor/cg_teamcolor
	}
}

models/players/ship4/ship4bodybrightskin
{
	{
		map models/players/ship4/ship4
		rgbgen lightingdiffuse
	}
	{
		map models/players/ship4/ship4bright
		blendfunc blend
		//alphafunc ge128		// may need this
		rgbgen entity			// cg_enemycolor/cg_teamcolor
	}
}

//=====================================================================
// weapons
//=====================================================================

models/weapons2/shells/m_shell
{
	{
		map models/weapons2/shells/m_shell.png
		rgbgen lightingdiffuse
	}
	{
		stage normalmap
		map models/weapons2/shells/m_shell_n.png
	}
	{
		stage specularmap
		map models/weapons2/shells/m_shell_s.png
	}

}

models/weapons2/minigun/minigun_flash
{
	cull none
	sort nearest
	{
		map models/weapons2/minigun/minigun_flash
		blendfunc add
		//rgbgen entity
	}
}

//=====================================================================
// weaphits
//=====================================================================

models/weaphits/shockwave
{
	cull none
	{
		map $whiteimage
		rgbgen entity
		alphagen entity
		blendfunc blend
	}
}

//=====================================================================
// mapmeshes
//=====================================================================

models/mapmeshes/corinthian
{
	
	surfaceparm detail
	surfaceparm nonsolid
	q3map_splotchFix
	q3map_lightmapsampleoffset 8
	{
		map models/mapmeshes/corinthian
		rgbgen identity
	}
	{
		stage specularmap
		map textures/basic/marble1_s
	}
	{
		map $lightmap
		blendfunc filter
	}
}
models/mapmeshes/corinthian_cm
{
	q3map_clipmodel
	qer_trans 0.0
	surfaceparm nodraw
}

models/mapmeshes/triglyph
{
	
	surfaceparm detail
	surfaceparm nonsolid
	q3map_lightmapsampleoffset 8
	{
		map models/mapmeshes/triglyph
		rgbgen identity
	}
	{
		map $lightmap
		blendfunc filter
	}
}

models/mapmeshes/doric1
{
	
	surfaceparm detail
	surfaceparm nonsolid
	q3map_lightmapsampleoffset 8
	{
		map models/mapmeshes/doric1
		rgbgen identity
	}
	{
		stage specularmap
		map textures/basic/marble1_s
	}
	{
		map $lightmap
		blendfunc filter
	}
}

models/mapmeshes/panel01
{
	
	{
		map models/mapmeshes/panel01
		rgbgen identity
	}
	{
		stage normalmap
		map models/mapmeshes/panel01_n
	}
	{
		stage specularmap
		map models/mapmeshes/panel01_s
	}
	{
		map $lightmap
		blendfunc filter
	}
}

models/powerups/redshieldgen/redshieldgen
{
	{
		map models/powerups/redshieldgen/shieldgen.tga
		rgbgen identity
	}
	{
		map $whiteimage
		rgbgen const ( 1 0 0 )
		blendfunc filter
	}
	{
		map textures/fx/sabfx1.tga
		blendfunc add
		tcGen environment
	}
}

models/powerups/redshieldgen/redshieldgen2
{
	{
		map models/powerups/redshieldgen/shieldgen.tga
		rgbgen identity
	}
	{
		map $whiteimage
		rgbgen const ( 1 0 0 )
		blendfunc filter
	}
	{
		map textures/fx/sabfx1.tga
		blendfunc add
		tcGen environment
	}
}

models/powerups/yellowshieldgen/yellowshieldgen
{
	{
		map models/powerups/redshieldgen/shieldgen.tga
		rgbgen identity
	}
	{
		map $whiteimage
		rgbgen const ( 1 1 0 )
		blendfunc filter
	}
	{
		map textures/fx/sabfx1.tga
		blendfunc add
		tcGen environment
	}
}
models/powerups/greenshieldgen/greenshieldgen
{
	{
		map models/powerups/redshieldgen/shieldgen.tga
		rgbgen identity
	}
	{
		map $whiteimage
		rgbgen const ( 0 1 0 )
		blendfunc filter
	}
	{
		map textures/fx/sabfx1.tga
		blendfunc add
		tcGen environment
	}

}
models/powerups/shieldblob/shieldblob
{
	{
		map $whiteimage
		rgbgen const ( .1 .1 .1 )
	}
	{
		map textures/fx/sabfx3.tga
		blendfunc add
		tcGen environment
	}
}

models/powerups/shieldblob/shieldsphere
{
	cull none
	{
		map textures/fx/sabfx2.tga
		blendfunc blend
		tcGen environment
		depthwrite
		rgbgen identity
	}
}

models/powerups/25hp/spanner
{
	{
		map $whiteimage
		rgbgen const ( .1 .1 .1 )
	}
	{
		map textures/fx/sabfx3.tga
		blendfunc add
		tcGen environment
	}
}

models/powerups/5hp/5hp
{
	{
		map $whiteimage
		rgbgen const ( .1 .1 .1 )
	}
	{
		map textures/fx/sabfx3.tga
		blendfunc add
		tcGen environment
	}
}

models/powerups/refit/refit
{
	{
		map models/powerups/refit/refit.tga
		blendfunc blend
	}
	{
		map models/powerups/refit/refitfx.tga
		blendFunc GL_ONE GL_ONE
		tcMod scroll 7.4 0.8
	}
	{
		map textures/fx/sabfx1.tga
		blendfunc add
		tcGen environment
	}
}

models/powerups/quad/quad
{
	{
		map models/powerups/quad/quad.tga
	}
	{
		map textures/fx/sabfx3.tga
		blendfunc add
		tcGen environment
	}
}

models/powerups/ammo/heminigunam
{
	{
		map $whiteimage
		rgbgen const ( .8 .25 .1 )
	}
	{
		map textures/fx/sabfx3.tga
		blendfunc add
		tcGen environment
	}
}

models/powerups/ammo/lightningam
{
	{
		map $whiteimage
		rgbgen const ( .1 .45 .7 )
	}
	{
		map textures/fx/sabfx3.tga
		blendfunc add
		tcGen environment
	}
}

models/powerups/ammo/machinegunam
{
	{
		map $whiteimage
		rgbgen const ( .6 .6 .1 )
	}
	{
		map textures/fx/sabfx3.tga
		blendfunc add
		tcGen environment
	}
}

models/powerups/ammo/plasmaam
{
	{
		map $whiteimage
		rgbgen const ( .7 .1 .7 )
	}
	{
		map textures/fx/sabfx3.tga
		blendfunc add
		tcGen environment
	}
}

models/powerups/ammo/railgunam
{
	{
		map $whiteimage
		rgbgen const ( .1 .7 .1 )
	}
	{
		map textures/fx/sabfx3.tga
		blendfunc add
		tcGen environment
	}
}

models/powerups/ammo/shotgunam
{
	{
		map $whiteimage
		rgbgen const ( .6 .45 .1 )
	}
	{
		map textures/fx/sabfx3.tga
		blendfunc add
		tcGen environment
	}
}

models/powerups/ammo/rocketam
{
	{
		map $whiteimage
		rgbgen const ( .8 .1 .1 )
	}
	{
		map textures/fx/sabfx3.tga
		blendfunc add
		tcGen environment
	}
}

models/powerups/ammo/grenadeam
{
	{
		map $whiteimage
		rgbgen const ( .1 .8 .1 )
	}
	{
		map textures/fx/sabfx3.tga
		blendfunc add
		tcGen environment
	}
}

models/powerups/ammo/homingam
{
	{
		map $whiteimage
		rgbgen const ( .7 .15 .15 )
	}
	{
		map textures/fx/sabfx3.tga
		blendfunc add
		tcGen environment
	}
}

models/weapons2/grenadel/grenadel
{
	{
		map models/weapons2/grenadelgrenadel.tga
		rgbgen lightingdiffuse
	}
}

models/weapons2/heminigun/heminigun
{
	{
		map models/weapons2/heminigun/heminigun.tga
		rgbgen lightingdiffuse
	}
}

models/weapons2/melee/melee
{
	{
		map models/weapons2/melee/melee.tga
		rgbgen lightingdiffuse
	}
}

models/weapons2/lightning/lightning
{
	{
		map models/weapons2/lightning/lightning.tga
		rgbgen lightingdiffuse
	}
}

models/weapons2/minigun/minigun
{
	{
		map models/weapons2/minigun/minigun.tga
		rgbgen lightingdiffuse
	}
}

models/weapons2/nanoid/nanoid
{
	{
		map models/weapons2/nanoid/nanoid.tga
		rgbgen lightingdiffuse
	}
}

models/weapons2/plasma/plasma
{
	{
		map models/weapons2/plasma/plasma.tga
		rgbgen lightingdiffuse
	}
}

models/weapons2/podgrenade/podgrenade
{
	{
		map models/weapons2/podgrenade/podgrenade.tga
		rgbgen lightingdiffuse
	}
}

models/weapons2/podrocket/podrocket
{
	{
		map models/weapons2/podrocket/podrocket.tga
		rgbgen lightingdiffuse
	}
}

models/weapons2/railgun/railgun
{
	{
		map models/weapons2/railgun/railgun.tga
		rgbgen lightingdiffuse
	}
}

models/weapons2/shotgun/shotgun
{
	{
		map models/weapons2/shotgun/shotgun.tga
		rgbgen lightingdiffuse
	}
}







