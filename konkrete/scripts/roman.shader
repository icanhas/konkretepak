textures/roman/trim1
{
	qer_editorimage textures/roman/trim1
	q3map_normalimage textures/roman/trim1_nh
	{
		map textures/roman/trim1
		rgbgen identity
	}
	{
		stage normalparallaxmap
		map textures/roman/trim1_nh
		normalscale 1 1
		parallaxdepth 0.1
	}
	{
		stage specularmap
		map textures/a/null_s
	}
	{
		map $lightmap
		blendfunc filter
	}
}

textures/roman/floor3
{
	qer_editorimage textures/roman/floor3
	q3map_normalimage textures/roman/floor3_nh
	{
		map textures/roman/floor3
		rgbgen identity
	}
	{
		stage normalparallaxmap
		map textures/roman/floor3_nh
	}
	{
		stage specularmap
		map textures/basic/marble1_s
	}
	{
		map $lightmap
		blendfunc filter
	}
}

textures/roman/wall1
{
	qer_editorimage textures/roman/wall1
	qer_normalimage textures/roman/wall1_nh
	{
		map textures/roman/wall1
		rgbgen identity
	}
	{
		stage normalparallaxmap
		map textures/roman/wall1_nh
	}
	{
		stage specularmap
		map textures/a/null_s
	}
	{
		map $lightmap
		blendfunc filter
	}
}
