textures/fx/tele1
{
	surfaceparm detail
	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm nomarks
	{
		map textures/fx/tele1
		rgbgen identity
		blendfunc add
		tcmod rotate 3
		tcmod stretch sin .3 1 1.1 0 .3
		tcmod scroll 1 1
	}
	{
		map textures/fx/tele1
		rgbgen identity
		blendfunc add
		tcmod rotate 40
		tcmod stretch sin .1 2 1.1 0 .02
		tcmod scroll -.03 .03
	}
}

textures/fx/ffield
{
	qer_editorimage textures/fx/ffield/ffield_0000.tga
	cull disable
	surfaceparm nonsolid
	surfaceparm trans
	surfaceparm nomarks
	{
		animmap 15 textures/fx/ffield/ffield_0000.tga textures/fx/ffield/ffield_0001.tga textures/fx/ffield/ffield_0002.tga textures/fx/ffield/ffield_0003.tga textures/fx/ffield/ffield_0004.tga textures/fx/ffield/ffield_0005.tga textures/fx/ffield/ffield_0006.tga textures/fx/ffield/ffield_0007.tga textures/fx/ffield/ffield_0008.tga textures/fx/ffield/ffield_0009.tga textures/fx/ffield/ffield_0010.tga textures/fx/ffield/ffield_0011.tga textures/fx/ffield/ffield_0012.tga textures/fx/ffield/ffield_0013.tga textures/fx/ffield/ffield_0014.tga 
		blendfunc add
	}
}