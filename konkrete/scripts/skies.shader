textures/skies/earthsky
{
	qer_editorimage env/earthsky/earthsky_bk.tga
	surfaceparm nomarks
	surfaceparm sky
	surfaceparm nolightmap
	surfaceparm noimpact
	skyparms env/earthsky/earthsky - -
}

textures/skies/kctf4sky
{
	qer_editorimage env/kctf4sky/kctf4sky_bk.tga
	surfaceparm nomarks
	surfaceparm sky
	surfaceparm nolightmap
	surfaceparm noimpact
	skyparms env/kctf4sky/kctf4sky - -
}
