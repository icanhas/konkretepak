// map shaders

textures/basic/marble2
{
	q3map_normalimage textures/basic/marble2_n
	{
		map textures/basic/marble2
		rgbgen identity
	}
	{
		tcgen lightmap
		map $lightmap
		rgbgen identity
		blendfunc filter
	}
}

textures/basic/concrete
{
	qer_editorimage textures/basic/concrete1
	q3map_normalimage textures/basic/concrete1_n
	{
		map textures/basic/concrete1
		rgbgen identity
	}
	{
		map $lightmap
		blendfunc filter
	}
	{
		stage normalmap
		map textures/basic/concrete1_n
	}
}

textures/basic/tile
{
	qer_editorimage textures/basic/gdgrey.tga
	{
		map textures/basic/gdgrey.tga
		rgbgen identity
	}
	{
		stage normalparallaxmap
		map textures/basic/tile_n.tga
	}
	{
		stage specularmap
		map textures/basic/tile_s.tga
	}
	{
		map $lightmap
		blendfunc filter
	}
}

textures/basic/glass
{
	qer_editorimage textures/basic/glass.tga
	qer_editortrans 0.5
	surfaceparm trans
	{
		map textures/basic/glass.tga
		rgbgen identity
		blendfunc blend
	}
	{
		stage specularmap
		map textures/basic/glass_s.tga
	}
	{
		map $lightmap
		blendfunc filter
	}
}

