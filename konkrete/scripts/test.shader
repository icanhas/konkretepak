models/testmask
{
	cull none
	
	{
		map models/test
		blendfunc GL_DST_COLOR GL_SRC_COLOR
		//alphafunc ge128
		//depthwrite
		rgbgen identity
	}
	{
		map models/testmask
		//blendfunc GL_DST_COLOR GL_ZERO
		//blendfunc GL_DST_COLOR GL_ZERO
		//alphafunc gt0
		blendfunc GL_DST_COLOR GL_SRC_COLOR
		//depthwrite
		rgbgen identity
	}
}

models/test
{
	cull none
	{
		map models/test
		blendfunc GL_DST_COLOR GL_ZERO
		//alphafunc ge128
		//depthwrite
	}
}

textures/decals/test
{
	cull none
	polygonoffset
	nomipmaps
	sort banner
	{
		clampmap textures/decals/test
		alphafunc ge128
		tcmod turb .1 .1 .1 .1
		rgbgen wave sin .5 .5 0 1.5
	}
	{
		map $lightmap
		blendfunc filter
	}
}

textures/test/pbrtest
{
	qer_editorimage textures/test/pbr_d
	q3map_globaltexture
	{
		stage diffusemap
		map textures/test/pbr_d
	}
	{
		map $lightmap
		blendfunc filter
	}
	{
		stage specularmap
		map textures/test/pbr2_s
		//specularscale .56 .8
		//specularreflectance 0.51
		//specularexponent 0.89
		roughness 0.3
	}
}

textures/test/nonpbrtest
{
	qer_editorimage textures/test/nonpbr_d
	//q3map_globaltexture
	{
		stage diffusemap
		map textures/test/nonpbr_d
	}
	{
		map $lightmap
		blendfunc filter
	}
	{
		stage specularmap
		map textures/test/nonpbr_s
		//specularreflectance .7
		//specularexponent 64
		gloss 1
		roughness .9
	}
}

textures/test/surfacelight
{
	qer_editorimage textures/test/surfacelight.png
	q3map_surfacelight 500000
	q3map_lightimage textures/test/surfacelight.png
	{
		map textures/test/surfacelight.png
		rgbgen identity
	}
}
